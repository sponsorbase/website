﻿<?php
$field_name = $_POST['cf_name'];
$field_email = $_POST['cf_email'];
$field_type = $_POST['type'];

if(!isset($field_name) || !isset($field_email) || !isset($field_type)){
  die("Udfyld venligst alle felter");
}

$mail_to = 'aske@mottelson.dk, gjedde-nielsen@hotmail.com, thomas.gjedde@gmail.com, peter.tvorup.sassersen@hotmail.com, nichlaskvist@hotmail.com';
$subject = 'Ny tilmelding til SponsorBase';

$body_message = 'From: '.$field_name."\n";
$body_message .= 'E-mail: '.$field_email."\n";
$body_message .= 'Type: '.$field_type."\n";

$headers = 'From: '.$field_email."\r\n";
$headers .= 'Reply-To: '.$field_email."\r\n";

$mail_status = mail($mail_to, $subject, $body_message, $headers);

if ($mail_status) { ?>
	<script language="javascript" type="text/javascript">
		alert('Tak for din interesse i SponsorBase. Vi vender tilbage snarest.');
		window.location = 'index.html';
	</script>
<?php
}
else { ?>
	<script language="javascript" type="text/javascript">
		alert('Der var et problem i din tilmeldning, prøv gerne igen');
		window.location = 'index.html';
	</script>
<?php
}
?>
